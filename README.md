DEMO-ChartJS
=========
Ejemplos de uso de la libreria chart.js

Se utiliza para su desarollo y pruebas:

- HTML.
- CSS.
- Javascript.

Temas que se aplican
====================

- Manejo correcto de etiquetas HTML5.
- Uso de reglas CSS3.
- Lógica de programación en JS y buenas prácticas.
- Uso de libreria chart.js.

Uso
===

- Colocar el proyecto dentro del servidor.
- Acceder con un navegador a la carpeta chartjs.

Contenido
=========
- line-static: Gráfico de lineas estático.
- line-json: Gráfico de lineas con información obtenida desde un JSON.
- line-animate: Gráfico de lineas con información obtenida desde un JSON e inserción de datos animada.

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- https://mattprofe.com.ar
